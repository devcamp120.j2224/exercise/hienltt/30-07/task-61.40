package com.devcamp.devcamporder_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.devcamporder_api.model.Product;

public interface IProductRepository extends JpaRepository<Product, Long>{
    
}
