package com.devcamp.devcamporder_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.devcamporder_api.model.Customer;

public interface ICustomerRepository extends JpaRepository<Customer, Long>{
    Customer findByUserId(long userId);
}
