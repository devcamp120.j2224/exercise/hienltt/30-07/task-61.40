package com.devcamp.devcamporder_api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.devcamporder_api.model.Order;

public interface IOrderRepository extends JpaRepository<Order, Long>{
    Order findByOrderId(long orderId);
}
