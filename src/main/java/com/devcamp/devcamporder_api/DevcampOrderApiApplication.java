package com.devcamp.devcamporder_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevcampOrderApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevcampOrderApiApplication.class, args);
	}

}
