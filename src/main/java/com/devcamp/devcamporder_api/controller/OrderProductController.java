package com.devcamp.devcamporder_api.controller;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.devcamporder_api.model.*;
import com.devcamp.devcamporder_api.repository.ICustomerRepository;
import com.devcamp.devcamporder_api.repository.IOrderRepository;

@RestController
@CrossOrigin
public class OrderProductController {
    @Autowired
    ICustomerRepository iCustomerRepository;
    @Autowired
    IOrderRepository iOrderRepository;


    @GetMapping("/devcamp-orders")
    public ResponseEntity<Set<Order>> getOrderList(@RequestParam(value = "userId", required = true) long userId){
        try {
            Customer customer = iCustomerRepository.findByUserId(userId);
            if(customer != null){
                return new ResponseEntity<Set<Order>>(customer.getOrders(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp-products")
    public ResponseEntity<Set<Product>> getProductList(@RequestParam(value = "orderId", required = true) long orderID){
        try {
            Order order = iOrderRepository.findByOrderId(orderID);
            if(order != null){
                return new ResponseEntity<Set<Product>>(order.getProducts(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
